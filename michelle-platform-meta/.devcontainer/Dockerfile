FROM my-nexus.MEM.pri:8090/MEM/vscode-node:debian

ARG DOCKER_COMPOSE_VERSION=1.26.2
ARG DOCKER_COMPOSE_SHASUM=13e50875393decdb047993c3c0192b0a3825613e6dfc0fa271efed4f5dbdd6eb

RUN DEBIAN_FRONTEND=noninteractive \
  && apt-get update \
  && apt-get install -y --no-install-recommends \
    groff \
    gnupg \
    less \
    lsb-release \
    openjdk-11-jre-headless \
    python3 \
    python3-pip \
    software-properties-common \
    zip \
  #
  # AWSCLI
  && pip3 install setuptools wheel \
  && pip3 install awscli \
  #
  # serverless
  && npm install --global \
    cdk \
    jest \
    meta \
    serverless \
  #
  # Docker cli & compose
  && curl -fsSL https://download.docker.com/linux/debian/gpg | apt-key add - \
  && add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/debian \
   $(lsb_release -cs) \
   stable" \
  && sudo curl -fsSL "https://github.com/docker/compose/releases/download/${DOCKER_COMPOSE_VERSION}/docker-compose-Linux-x86_64" -o /usr/local/bin/docker-compose \
  && echo "${DOCKER_COMPOSE_SHASUM}  /usr/local/bin/docker-compose" | sha256sum -c - \
  && chmod +x /usr/local/bin/docker-compose \
  && apt-get update \
  && apt-get install -y docker-ce-cli
  