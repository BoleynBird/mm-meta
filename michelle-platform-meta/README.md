michelle-platform-meta
=====================

This repository allows developers to easily create a workspace with all the necessary tools for the development of the
michelle Platform project.  It contains references to all git repositories of the components of the project and a way to
check them out and start using them. The project works particuarly well in conjunction with Microsoft VSCode.

# Prerequisites

In order to use Git in the container, you need to configure the Windows SSH Agent with your SSH private key.

1. Open `services.msc` as an Administrator
1. Locate the "OpenSSH Authentication Agent" Service
1. Right Click and select "Properties"
1. Set the "Startup type" to "Automatic (Delayed Start)"
1. Ensure that only your user has access to your private key (https://superuser.com/questions/1296024/windows-ssh-permissions-for-private-key-are-too-open)
1. Run `ssh-add path\to\id_rsa`
1. Open Docker Desktop and go to "Settings" > "Docker Engine" and allow "my-nexus.MEM.pri:8090" within "insecure-registries" (see below)
   ```
   "insecure-registries": [
      "my-nexus.MEM.pri:8090"
   ],
   ```
1. Finally, within Docker Desktop "Settings" > "Resources" > "File Sharing", ensure that the "C" drive is checked. 

# Using the repository

The easiest way to use this repository is to clone it with Visual Studio Code, and open the folder with the Remote Containers extension.

When this starts, it will build a container with all the necessary tools for development. The first time it runs, it will also download all of the child Git repositories.

The first time that the folder is opened, you should decline to open the enclosed workspace, so that it builds the Docker container. Once this has been built,
reopen the window as a workspace. Due to a limitation of VS Code, the meta repository cannot be part of the workspace.

`meta` is an NPM package to help project use multiple Git repositories. To clone or pull the repositories, run `meta git update` from the root folder. Visual Studio Code will run this when the container is first created.

To add new repositories, change to the `/workspaces/michelle-platform-meta` parent directory and run `meta project import <path> <git-url>`. This will add the project to the
meta repository. The new repository should then be added to the code workspace using the File Menu's "Add folder to workspace" action. The resulting changes should be
committed and pushed to Git from the command line.

# Windows 7

The Remote Containers extension does not work with Docker Toolbox; the only version of Docker available for Windows 7. Additionally, it is not possible to use Docker
Toolbox whilst connected to the MEM VPN. Therefore, if working remotely, you must:

1. Clone the MEM-vscode-base repository (http://git.MEM.pri:7990/projects/DOCKER/repos/MEM-vscode-base/browse)
1. Build the vscode-base:debian image from the MEM-vscode-base repository:
   ```
   MEM-vscode-base# docker build -t my-nexus.MEM.pri:8090/MEM/vscode-base:debian -f base/debian/Dockerfile base
   ```
1. Build the vscode-node:debian image from the MEM-vscode-base repository:
   ```
   MEM-vscode-base# docker build -t my-nexus.MEM.pri:8090/MEM/vscode-node:debian -f node/debian/Dockerfile node
   ```
1. Back in this repository, build the devcontainer image:
   ```
   michelle-platform-meta# docker build -t hg-platform .
   ```
1. Install `meta` npm package globally
   ```
   npm install --global meta
   ```
1. Connect to the VPN and download the repositories from Git:
   ```
   meta git update
   ```
1. Now disconnect from the VPN and run the docker container
   ```
   docker run -ti \
     --name hg-platform \
     -v /$(pwd):/workspaces/michelle-platform-meta \
     -v /`cygpath $USERPROFILE`/.gitconfig:/home/vscode/.gitconfig \
     -w /workspaces/michelle-platform-meta \
     -u vscode \
     hg-platform
   ```
1. Open the michelle.code-workspace file in VS Code.

Because we did not specify `--rm` in the Docker run command, we can simply re-connect to the stopped container each day with
`docker start hg-platform`.
