@ECHO off
SETLOCAL

IF "%~1" == "" (
  ECHO Usage: %~0 profile-name ^[ role-name ^(default: MEM-Developer^) ^]>&2
  EXIT /B 10
)
SET PROFILE=%~1

IF "%~2" == "" (
  SET ROLE=MEM-Developer
) ELSE (
  SET ROLE=%~2
)

FOR /F "usebackq tokens=1" %%a IN (
  `aws sts get-caller-identity --profile %~1 ^| jq -r ".Account"`
) DO SET ACCOUNT=%%a

FOR /F "usebackq tokens=1,2,3" %%a IN (
  `aws sts assume-role --role-arn arn:aws:iam::%ACCOUNT%:role/%ROLE% --role-session-name %USERNAME% ^| ^
  jq -j ".Credentials.AccessKeyId, \" \", .Credentials.SecretAccessKey, \" \", .Credentials.SessionToken"`
) DO (
  ECHO. export AWS_ACCESS_KEY_ID=%%a
  ECHO. export AWS_SECRET_ACCESS_KEY=%%b
  ECHO. export AWS_SESSION_TOKEN=%%c 
)
