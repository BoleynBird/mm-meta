#!/bin/bash

if [ -z "$1" ]; then
  echo "Usage: $0 aws-profile-name [role-name (MEM-Developer)]">&2
  exit 1
fi

ROLE=${2:-MEM-Developer}

ACCOUNT=$(aws sts get-caller-identity --profile "$1" | jq -r '.Account')

CREDS=$(aws sts assume-role --role-arn arn:aws:iam::${ACCOUNT}:role/${ROLE} --role-session-name $USER)

echo " export AWS_ACCESS_KEY_ID=$( jq -r '.Credentials.AccessKeyId' <<< ${CREDS} )"
echo " export AWS_SECRET_ACCESS_KEY=$( jq -r '.Credentials.SecretAccessKey' <<< ${CREDS} )"
echo " export AWS_SESSION_TOKEN=$( jq -r '.Credentials.SessionToken' <<< ${CREDS} )"
